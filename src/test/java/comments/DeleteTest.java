package comments;

import io.restassured.http.ContentType;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;

public class DeleteTest {
    final String baseURL = "http://localhost:3000/comments";
    @Test
    void shouldDeleteExistingPost () {
        Map<String, Object> commentsData = new HashMap<String, Object>();
        commentsData.put("id", 3);
        commentsData.put("body", "opis slowny body");
        commentsData.put("postId", "3");
        int id = given().contentType(ContentType.JSON)
                .body(commentsData)
                .when()
                .post(baseURL)
                .body().jsonPath().get("id");
        System.out.println(id);
        when().delete(baseURL + "/" + id).then().statusCode(200);
    }

}
