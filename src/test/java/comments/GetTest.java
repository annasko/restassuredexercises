package comments;

import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.when;

public class GetTest {
    final String baseURL = "http://localhost:3000/comments";

    @Test
    void checkIfResponseStatusCodeIs200WhenGettingAllComments() {
        when().get(baseURL).then().statusCode(200);
    }
    @Test
    void checkIfResponseStatusCodeIs200WhenGettingID1Comment() {

        when().get(baseURL + "/1").then().statusCode(200);
    }

}
