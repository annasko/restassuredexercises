package comments;

import io.restassured.http.ContentType;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;

public class PostTest {
    final String baseURL = "http://localhost:3000/comments";

    @Test
    void shouldReturnCode200WhenSavedNewComments() {
        Map<String, Object> commentData = new HashMap<String, Object>();
        commentData.put("id", "3");
        commentData.put("body", "opis slowny body");
        commentData.put("postId", "3");
        given().contentType(ContentType.JSON)
                .body(commentData)
                .when()
                .post(baseURL)
                .then()
                .statusCode(201);
    }


}
