package comments;

import io.restassured.http.ContentType;
import io.restassured.mapper.ObjectMapper;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class PutTest {
    final String baseURL = "http://localhost:3000/comments";

    @Test
    void shouldBeAbleToEditARecord() {
        Map<String, Object> commentData = new HashMap<String, Object>();
        commentData.put("id", 77);
        commentData.put("body", "opis slowny body");
        commentData.put("postId", "4");

        int newCommentId = given()
                .contentType(ContentType.JSON)
                .body(commentData)
                .post(baseURL)
                .body().jsonPath().get("id");

        commentData.replace("body", "zmieniony opis slowny");
        System.out.println(newCommentId);
        given().contentType(ContentType.JSON)
                .body(commentData)
                .when()
                .put(baseURL + "/" + newCommentId).then().statusCode(200)
                .and()
                .body("body", equalTo("zmieniony opis slowny"));


    }
}
